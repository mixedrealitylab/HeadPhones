/*
Copyright (c) 2017 Jens Grubert (jg@jensgrubert.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package de.mixedrealitylab.headphones;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.FloatMath;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.amazon.headtracking.HeadTrackingEvent;
import com.amazon.headtracking.HeadTrackingListener;
import com.amazon.headtracking.HeadTrackingManager;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity implements HeadTrackingListener, SensorEventListener  {
    static final String TAG = "UYH";
    String _ipAddress = "NA";
    //***** webview variables *****
    Timer timer = new Timer();
    long count = 0;
    private WebView         mWebView; // for showing the apps
    private String 			mStrToWv;
    private String mViewURL;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    float[] mAccelerationValues = new float[3];
    private float mAccel = 0.00f;
    private float mAccelCurrent = SensorManager.GRAVITY_EARTH;
    private float mAccelLast = SensorManager.GRAVITY_EARTH;


    private boolean	mWebpageLoaded = false;

    //***** head tracking variables *****
    private HeadTrackingManager mHeadTrackingManager;
    
    float  mHeadPosX, mHeadPosY, mHeadPosZ;



    boolean isMoving = true;
    boolean isTracked = false;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("Hello", "hello");

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {

            mViewURL = extras.getString("mViewURL");
            Log.d("url" ,mViewURL);

            String screenOrientation = extras.getString("orientation");
            Log.d("screenOrientation" ,screenOrientation);

            if(screenOrientation.equals("portrait")) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }

        mSensorManager = (SensorManager) getSystemService
                (Context.SENSOR_SERVICE);

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        

        initWebView();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                count = 0;
            }
        },  1000 , 1000);
        initHeadTracking();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onDestroy() {
        super.onDestroy();
    }

	//***** webview methods *****
	// initializing the web view
    protected void initWebView() {
        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setAllowFileAccessFromFileURLs
                (true);
        mWebView.getSettings
                ().setAllowUniversalAccessFromFileURLs(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int
                    progress) {
            }
            public boolean onConsoleMessage(ConsoleMessage msg) {
                Log.d("HEADPHONES", msg.message() + " -- From line "
                        + msg.lineNumber() + " of "
                        + msg.sourceId());
                return true;
            }
        });
        mWebView.setWebViewClient(
                new WebViewClient(){
                    public boolean shouldOverrideUrlLoading(WebView
                                                                    view, String url){
                        mWebpageLoaded = false;
                        view.loadUrl(url);
                        return false;
                    }
                    public void onPageFinished(WebView view, String
                            url){
                        mWebpageLoaded = true;

                        Log.d(TAG, "webpage loaded");
                        _ipAddress = getIpAddress();
                        mWebView.post(WebViewMessageSetIP);                     
                    }
                });

        mWebView.addJavascriptInterface(new WebAppInterface (this), "Android");
        mWebView.setVisibility(View.VISIBLE);
        mWebView.loadUrl(mViewURL);
    }

    /// injects the head tracking data into the webpage
    private Runnable WebViewMessagePostHeadPose = new Runnable()
    {
        @Override
        public void run() {            
            mWebView.loadUrl("javascript:headPose(" + mHeadPosX
                    + ", "  + mHeadPosY  + ", " + mHeadPosZ + ", " + 0 +
                    ", " + 0 + ", " + 0 + ")");
        }
    };
    /// injects the client ip address into the webpage
    private Runnable WebViewMessageSetIP = new Runnable() {
        @Override
        public void run() {
            mWebView.loadUrl("javascript:setIpAddress('"+
                    _ipAddress + "')");
        }
    };

    /// registers  the client with the server
    private Runnable WebViewMessageRegisterDevice = new Runnable() {
		@Override
		public void run() {
			mWebView.loadUrl("javascript:registerDevice()");
		}
	};

    private Runnable WebViewMessageSetIsMoving = new Runnable() {
		@Override
		public void run() {
			mWebView.loadUrl("javascript:isMovingf(" +isMoving + ")");
		}
	};

    /// receiving data from the webpage
    public class WebAppInterface {
        Context mContext;
        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }
        /** Show a toast from the web page */
        @JavascriptInterface
        public void showToast(String toast) {
            Log.i(TAG, "WEBVIEW: " + toast);
            Toast.makeText(mContext, toast,
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected String getIpAddress() {
       //WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiManager wm = (WifiManager)  getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        Log.d(TAG, "ip: " + ip);
        return ip;
    }

    //***** head tracking methods *****
    /// initialize the head tracker
    protected void initHeadTracking() {
        // Get the current HeadTrackingManager for this application.
        try {
            mHeadTrackingManager =
                    HeadTrackingManager.createInstance(this);      
            mHeadTrackingManager.registerListener(this);      
        } catch (IllegalArgumentException e) {      
            Log.e(TAG, "No HeadTrackingManager is available for this application", e);      
        }      
    }      
      
    /// callback to receive head tracking data      
    @Override      
    public void onHeadTrackingEvent(HeadTrackingEvent      
                                            headTrackingEvent) {      
        count++;      
        mHeadPosX = headTrackingEvent.x_mm;      
        mHeadPosY = headTrackingEvent.y_mm;
        mHeadPosZ = headTrackingEvent.z_mm;

       
        double lastpy = mHeadPosY;
        double lastpx = mHeadPosX;

        if(headTrackingEvent.isTracking && headTrackingEvent.isFaceDetected){
            isTracked = true;
        }
        if(mWebpageLoaded) {
            mWebView.post(WebViewMessagePostHeadPose);
        }
    }

  

    @Override
    protected void onResume() {

        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
        // You must implement this callback in your code.
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor sensor = event.sensor;

        int type = sensor.getType();
        //Log.d(TAG ,"TYPE " + type + " " + Sensor.TYPE_MAGNETIC_FIELD+" " +Sensor.TYPE_ACCELEROMETER  );
        switch (type) {
            case Sensor.TYPE_ACCELEROMETER:
                for(int i=0; i<3; i++){
                    mAccelerationValues[i] =  event.values[i];
                    Log.d(TAG , mAccelerationValues[i]+" " + i);

                }
                float x = mAccelerationValues[0];
                float y = mAccelerationValues[1];
                float z = mAccelerationValues[2];

                mAccelLast = mAccelCurrent;
                mAccelCurrent = FloatMath.sqrt(x * x + y * y + z * z);
                float delta = mAccelCurrent - mAccelLast;
                mAccel = mAccel * 0.9f + delta;
                // Make this higher or lower according to how much
                // motion you want to detect
                if(mAccel > 0.09){
                    // do something
                    isMoving = true;
                    Log.d(TAG , "DDD");
                }else{
                    if(!isTracked){
                        isMoving = true;
                    }else{
                        isMoving = false;
                    }
                }
                mWebView.post(WebViewMessageSetIsMoving);

                break;

        }

    }

}




