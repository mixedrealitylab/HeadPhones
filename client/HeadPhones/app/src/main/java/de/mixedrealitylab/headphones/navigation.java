/*
Copyright (c) 2017 Jens Grubert (jg@jensgrubert.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package de.mixedrealitylab.headphones;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

public class navigation extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        Button maplayers = (Button) findViewById(R.id.mapLayers);
        maplayers.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String mViewURL = "file:///android_asset/mapslayerssmooth.html";
               // String mViewURL = "file:///android_asset/mapsmooth.html";
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("mViewURL", mViewURL);
                intent.putExtra("orientation", "landscape");
                startActivity(intent);
            }
        });










    }

}
