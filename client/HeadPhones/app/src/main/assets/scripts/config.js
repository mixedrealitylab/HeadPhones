/*
Copyright (c) 2017 Jens Grubert (jg@jensgrubert.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


var wsServerURL = "ws://192.168.0.100:3001"; 

// Amazon fire phone dimensions in mm
var fpWidthMm = 67;
var fpHeightMm = 138;
var fpDisplayWidthMm = 58; 
var fpDisplayHeightMm = 103;

// the size of the Amazon fire phone webview in px
var fpWvCanvasWidthPx = 360;
var fpWvCanvasHeightPx = 640; 