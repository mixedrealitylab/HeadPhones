This software is distributed under the GNU General Public License version 3, see license.txt.
contact: Jens Grubert, jg@jensgrubert.de

Further information: http://bit.ly/MRLheadphones, http://www.mixedrealitylab.de

This software version supports Amazon Fire Phones and has a single demo application showing layered maps.

If you use this software for publications please cite:

Jens Grubert and Matthias Kranz. 2017. HeadPhones: Ad Hoc Mobile Multi-Display Environments through Head Tracking. 
In Proceedings of the 2017 CHI Conference on Human Factors in Computing Systems (CHI '17). 
ACM, New York, NY, USA, 3966-3971. 
DOI: https://doi.org/10.1145/3025453.3025533

To get this software running you will need at least two Amazon Fire Phones, Android Studio, Node.js and the Amazon Fire Phone API
(https://developer.amazon.com/public/solutions/devices/fire-phone/overview/getting-started-developing-apps-for-fire-phone)

Further you will need to edit:
"\client\Headphones\app\src\main\assets\scripts\config.js" to update the websocket server URI
(wsServerURL)
Also, check if you are in an ipv4 or ipv6 network and regard the comments on line 15-16 in "server\server.js"

The software is divided into a client application (Android Studio project, in the "client" directory) and a server application (node.js, in the "server" directory).
To start the software, first connect all devices into a common network.
Then start the node.js server component ("node server.js" in the "server" directory).
Finally upload and run the Android applications.


If you want to use other devices than the Amazon Fire Phone, you need to replace the head tracker (e.g., use OpenCV or contact Jens Grubert) and update the phone dimensions in config.js.