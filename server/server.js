/*
Copyright (c) 2017 Jens Grubert (jg@jensgrubert.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// disable this flag if you are in a ipv4 network only. In an ipv6 network the ipadress is preceeded by "::ffff:".
// see http://stackoverflow.com/questions/29411551/express-js-req-ip-is-returning-ffff127-0-0-1
var ipv6 = true;

var glMatrix = require('gl-matrix');

/// websocket server
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({port: 3001});
wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    client.send(data);
  });
};



wss.on('connection', function(ws) {
    ws.on('message', function(data) {
     
	var client_ip = ws._socket.remoteAddress;
	 // console.log("client_ip: " + client_ip);
	
	if(ipv6 == true) {
		// get rid of "::ffff:"
		var iparr = client_ip.split(":");
		client_ip = iparr[3]; 
	}
	//console.log("client_ip: " + client_ip);
        var jso = JSON.parse(data);
        if(jso.action == "registerDevice") {
            registerDevice(client_ip);  
 	    //console.log(data);
        } else if(jso.action == "setHeadTrackingData") {
            setHeadTrackingData(client_ip, jso.px, jso.py, jso.pz, jso.rx, jso.ry, jso.rz);
        } else if(jso.action == "setCOSOrigin") {
            setCOSOrigin(client_ip);
        } else if(jso.action == "getGlobalDevicePosition") {
            getGlobalDevicePosition(client_ip);
        } else if(jso.action == "deviceTouchPanMove" || jso.action == "deviceTouchPanStart" || jso.action == "deviceTouchPanEnd") {
            wss.broadcast(data);
        }
       
		
    });
});
 
 


var _devices = [];
var _deviceCOSOrigin; // the device with the COS origin

var _deviceHTD = {};

var _globalCOSOriginSet = false;
var registerDevice = function(ipAddress) {
	//console.log("iparr: " + iparr[3]); 
    console.log("added ip " + ipAddress);
    _devices.push(ipAddress);
    if(_devices.length == 1) {
        setCOSOrigin(ipAddress);
    }
    
};


var degToRad = function(deg) {
    var rad = deg/180.0*Math.PI;
    return rad;
};

// assumes radians
var euler123ToQuat = function(rx,ry,rz) {
    
    qw = (Math.cos(rx/2) * Math.cos(ry/2) * Math.cos(rz/2)) + (Math.sin(rx/2) * Math.sin(ry/2) * Math.sin(rz/2) );
    qx = (-Math.cos(rx/2) * Math.sin(ry/2) * Math.sin(ry/2)) + ( Math.cos(ry/2) * Math.cos(rz/2) * Math.sin(rx/2) );
    qy = ( Math.cos(rx/2) * Math.cos(rz/2) * Math.sin(ry/2) ) + (Math.sin(rx/2)  * Math.cos(ry/2) * Math.sin(rz/2) );
    qz = (Math.cos(rx/2)  * Math.cos(ry/2) * Math.sin(rz/2) ) - (Math.sin(rx/2) * Math.cos(rz/2)  * Math.sin(ry/2) );
    
    var q = glMatrix.quat.fromValues(qx,qy,qz,qw);
    return q;
    
};

var quatToEuler123 = function(qx, qy, qz, qw) {
    var rx = Math.atan2(2*qy*qz + 2*qw*qx, qz*qz - qy*qy - qx*qx + qw*qw);
    var ry = -Math.asin(2*qx*qz - 2*qw*qy);;
    var rz = Math.atan2(2*qx*qy + 2*qw*qz, qx*qx + qw*qw - qz*qz -qy*qy);

    var euler = [rx, ry, rz];
    return euler;
};

var azimuth = 0;
var pitch = 0;
var roll = 0;
var rotationMatrix = 0;

var oldValueX = [];
var oldValueY = [];
var oldValueZ = [];
var alpha = 0.9;

var deviceOrder = 0;

var exponentialSmoothing = function(value , oldValue){
  var newValue = alpha*oldValue + (1-alpha)*(value);
  return newValue;
}


var setHeadTrackingData = function(ipAddress, px, py, pz, rx, ry, rz) {
    
   // console.log("setHeadTrackingData : " + px + " " + py + " " + pz + " " + rx + " " + ry + " " + rz);
    // convert pose string into mat4
    var transV = glMatrix.vec3.fromValues(px, py, pz);

    // euler (rad) to quaternion
    var rq = euler123ToQuat(rx,ry,rz);
    
    // we do not use the rotation for the map application, but use a unit quat
    var uq = glMatrix.quat.create();
    
    var pose = glMatrix.mat4.create();
    
   pose = glMatrix.mat4.fromRotationTranslation(pose, uq, transV);
    //pose = glMatrix.mat4.fromRotationTranslation(pose, rq, transV);
    
   // console.log("pose: " + pose);
    _deviceHTD[ipAddress] = pose;
};

var setCOSOrigin = function(ipAddress) {
    _deviceCOSOrigin = ipAddress;
    _globalCOSOriginSet = true;
    console.log("COSOrigin: " + _deviceCOSOrigin);
};


var getGlobalDevicePosition = function(ipAddress) {   
    if(_globalCOSOriginSet == false) {
        return;
    }
    
    var localHeadPose = _deviceHTD[ipAddress];
    var cosOriginHeadPose = _deviceHTD[_deviceCOSOrigin];
       
    //console.log("localHeadPose: " + localHeadPose);
    //console.log("cosOriginHeadPose: " + cosOriginHeadPose);
    
    var localHeadPoseInv = glMatrix.mat4.create();
    localHeadPoseInv = glMatrix.mat4.invert(localHeadPoseInv, localHeadPose);
     
    var pose = glMatrix.mat4.create();
    
    pose = glMatrix.mat4.multiply(pose, cosOriginHeadPose, localHeadPoseInv);
        
    // column major order
    var px = pose[12]; var py = pose[13]; var pz = pose[14];

    if(oldValueX[ipAddress] == null){
        oldValueX[ipAddress] = px;

    }else{
         px = exponentialSmoothing(px, oldValueX[ipAddress]);
         oldValueX[ipAddress] = px;
    }

    if(oldValueY[ipAddress] == null){
        oldValueY[ipAddress] = py;
    }else{
        py = exponentialSmoothing(py, oldValueY[ipAddress]);
        oldValueY[ipAddress] = py;
    }

    if(oldValueZ[ipAddress] == null){
        oldValueZ[ipAddress] = pz;
    }else{
        pz = exponentialSmoothing(pz, oldValueZ[ipAddress]);
        oldValueZ[ipAddress] = pz;
    } 
    
    //console.log("ipAddress, px,py,pz " + ipAddress + " "  + px + " " + py + " " + pz);
    
    var rotMat = glMatrix.mat3.create(); rotMat = glMatrix.mat3.fromMat4(rotMat, pose);
    var q = glMatrix.quat.create(); q = glMatrix.quat.fromMat3(q, rotMat);
    
    var euler = quatToEuler123(q[0], q[1], q[2], q[3]);
    var rx = Math.floor(euler[0]); var ry = Math.floor(euler[1]); var rz = Math.floor(euler[2]);
    

    var data = '{"action": "getGlobalDevicePosition", "target": "' +  ipAddress + '", "px": "' +  px + '", "py": "' +  py + '", "pz": "' +  pz + '", "rx": "' +  rx + '", "ry": "' +  ry + '", "rz": "' +  rz + '"    }';
    
    wss.broadcast(data);    
    
};




